package machine;

import java.util.Scanner;


public class CoffeeMachine {
    public static void main(String[] args) {

        int waterAmount = 400;
        int milkAmount = 540;
        int coffeeAmount = 120;
        int diposableCups = 9;
        int money = 550;

        String optionChosen = "";

        Scanner sc = new Scanner(System.in);
        System.out.println("Write action (buy, fill, take, remaining, exit): ");
        while (sc.hasNext()) {

            optionChosen = sc.nextLine();
            if (!optionChosen.equals("exit")) {
                switch (optionChosen) {
                    case "buy":
                        System.out.println("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino: ");

                        String coffeeChosen = sc.nextLine();
                        switch (coffeeChosen) {
                            case "1":
                                if (waterAmount >= 250) {
                                    if (coffeeAmount >= 16) {
                                        if (diposableCups > 0) {
                                            System.out.println("I have enough resources, making you a coffee!\n");
                                            waterAmount = waterAmount - 250;
                                            coffeeAmount = coffeeAmount - 16;
                                            money = money + 4;
                                            diposableCups = diposableCups - 1;
                                        } else {
                                            System.out.println("Sorry, not enough cups!");
                                        }
                                    } else {
                                        System.out.println("Sorry, not enough coffee!");
                                    }
                                } else {
                                    System.out.println("Sorry, not enough water!");
                                }
                                System.out.println("Write action (buy, fill, take, remaining, exit): ");
                                break;
                            case "2":
                                if (waterAmount >= 350) {
                                    if (milkAmount >= 75) {
                                        if (coffeeAmount >= 20) {
                                            if (diposableCups > 0) {
                                                System.out.println("I have enough resources, making you a coffee!\n");
                                                waterAmount = waterAmount - 350;
                                                milkAmount = milkAmount - 75;
                                                coffeeAmount = coffeeAmount - 20;
                                                money = money + 7;
                                                diposableCups = diposableCups - 1;

                                            } else {
                                                System.out.println("Sorry, not enough cups!");
                                            }
                                        } else {
                                            System.out.println("Sorry, not enough coffee!");
                                        }
                                    } else {
                                        System.out.println("Sorry, not enough milk!");
                                    }
                                } else {
                                    System.out.println("Sorry, not enough water!");
                                }
                                System.out.println("Write action (buy, fill, take, remaining, exit): ");
                                break;
                            case "3":
                                if (waterAmount >= 200) {
                                    if (milkAmount >= 100) {
                                        if (coffeeAmount >= 12) {
                                            if (diposableCups > 0) {
                                                System.out.println("I have enough resources, making you a coffee!\n");
                                                waterAmount = waterAmount - 200;
                                                milkAmount = milkAmount - 100;
                                                coffeeAmount = coffeeAmount - 12;
                                                money = money + 6;
                                                diposableCups = diposableCups - 1;
                                            } else {
                                                System.out.println("Sorry, not enough cups!");
                                            }
                                        } else {
                                            System.out.println("Sorry, not enough coffee!");
                                        }
                                    } else {
                                        System.out.println("Sorry, not enough milk!");
                                    }
                                } else {
                                    System.out.println("Sorry, not enough water!");
                                }
                                break;
                            default:
                                System.out.println("wrong input");
                                System.out.println("Write action (buy, fill, take, remaining, exit): ");
                                //throw new RuntimeException("no such coffee: " + coffeeChosen);
                                continue;
                        }
                        break;
                    case "fill":
                        System.out.println("Write how many ml of water do you want to add:");
                        int waterToAdd = sc.nextInt();
                        waterAmount = waterAmount + waterToAdd;
                        System.out.println("Write how many ml of milk do you want to add:");
                        int milkToAdd = sc.nextInt();
                        milkAmount = milkAmount + milkToAdd;
                        System.out.println("Write how many grams of coffee beans do you want to add:");
                        int coffeeBeansToAdd = sc.nextInt();
                        coffeeAmount = coffeeAmount + coffeeBeansToAdd;
                        System.out.println("Write how many disposable cups of coffee do you want to add:");
                        int cupsToAdd = sc.nextInt();
                        diposableCups = diposableCups + cupsToAdd;
                        System.out.println("Write action (buy, fill, take, remaining, exit): ");
                        break;
                    case "take":
                        System.out.println("I gave you $" + money + "\n");
                        money = 0;
                        break;
                    case "remaining":
                        System.out.println("The coffee machine has:");
                        System.out.println(waterAmount + " of water");
                        System.out.println(milkAmount + " of milk");
                        System.out.println(coffeeAmount + " of coffee beans");
                        System.out.println(diposableCups + " of disposable cups");
                        System.out.println(money + " of money\n");
                        System.out.println("Write action (buy, fill, take, remaining, exit): ");
                        break;
                    default:
                        System.out.println("wrong input");
                        System.out.println("Write action (buy, fill, take, remaining, exit): ");
                }
            } else {
                System.out.println("Machine is turning off. BYE!");
                break;
            }
        }
    }
}






